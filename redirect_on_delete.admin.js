(function ($) {

  Drupal.behaviors.redirectOnDeleteAdmin = {
    attach: function (context) {
      $('#redirect-on-delete-preview', context)
        .html('<a href="#">' + Drupal.t('Open destination in new window') + '</a>')
        .bind('click', function() {
          var destination = $('#edit-redirect-destination', context).val();
          // Generate the link if the destination has a value.
          if (destination.length > 0) {
            // Replace special Drupal <front> value with site frontpage.
            destination = destination.replace('<front>', '');
            // Open link in new window.
            window.open(Drupal.urlIsLocal(destination) ? Drupal.settings.basePath + destination : destination);
            return false;
          }
        });
    }
  };

})(jQuery);
